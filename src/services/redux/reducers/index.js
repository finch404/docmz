import {default as user} from "./userReducer";
import {default as doctors} from "./doctorsReducer";
import {default as notifications} from "./notificationReducer";

export default {
    user,
    doctors,
    notifications
}