import {default as user} from "./userReducer";
import {default as doctors} from "./doctorsReducer";

export default {
    user,
    doctors,
}